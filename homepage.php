<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section id="hero" class="cover" style="background-image: url(<?php $image = get_field('hero_photo'); echo $image['url']; ?>);">
		
	</section>

	<section id="main">
			
		<section id="photos">

			<div class="gallery">
				<div class="grid col-1">
					<div class="col-2">
						<img src="<?php $image = get_field('left_column_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div class="col-2 stack">
						<img src="<?php $image = get_field('right_column_photo_1'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						<img src="<?php $image = get_field('right_column_photo_2'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>						
				</div>

				<div class="col-1 wide">
					<img src="<?php $image = get_field('wide_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

			</div>
			
		</section>

		<section id="info">
			
			<div class="description">
				<?php the_field('description'); ?>
			</div>

			<div class="hours details">
				<h3>Hours</h3>
				<p><?php the_field('hours', 'options'); ?></p>
			</div>

			<div class="location details">
				<h3>Location</h3>
				<p><?php the_field('location', 'options'); ?></p>
			</div>

		</section>

	</section>

<?php get_footer(); ?>