<?php include(locate_template('partials/header/global-variables.php')); ?>


<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
<link rel="stylesheet" href="https://use.typekit.net/vcw4kod.css">
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/heavy-restaurants/heavy-restaurants.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo $child_theme_path; ?>/<?php echo $css_filename; ?>.css?v=1" />
